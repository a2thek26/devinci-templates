# Devinci Templates

Create content types easily with Devinci Templates. 

## Installation

## Generators

This package comes with a number of helpful file generation commands to aid in quick development.

## Form Configuration

## Table Builder

## Table Widgets

A widget can be something as simple as wrapping the column value with tags in order to bold it or as complex as adding buttons that link to different places depending on other values.

There are two types of table widgets: 

1. Ones that replace/augment the value in a table column
2. Ones to be added before or after the defined values in a table row. 

Each widget class extends the `WidgetBase` class and has a required method called `render`. The render method accepts 3 arguments:

`$value` - The calculated value from the record

`$record` - The entire record

`$config` - A configuration array

The configuration is different depending on the widget that it represents. 

The Table Builder ships with a few widgets out of the box:

### Wrapper

Wrap a table value with some markup.

Possible config values:

- `open`  (string) The opening wrapper tag. default: "<span>"
- `close` (string) The closing wrapper tag. default: "</span>"

### Bold

This widget utilized the 'wrapper' widget by simply setting the config values as opening and closing **strong** tags.

*This widget accepts no configuration*

### Button

The button widget is the most complex of all the pre-packaged widgets.

### Date Format

### Money

### Boolean

Turn a boolean value into a reader-friendly value

Possible config values:

- `yes` (string) The text to display if the boolean is true. default: 'Yes'
- `no`  (string) The text to display if the boolean is false. default: 'No'
