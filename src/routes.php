<?php

Route::group([
	'prefix'    => config('templates.templates_uri', 'templates'),
	'namespace' => '\\Devinci\\Templates\\Controllers'], function(){

	// catch all routes coming within template prefix
	Route::any('{slug}', 'TemplateController@run')->where('slug', '(.*)?');

	// template index
	Route::get('/', 'IndexController@index');
});
