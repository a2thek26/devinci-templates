<?php

return [
	'widget' => [
		'boolean' => [
			'yes' => 'Yes',
			'no'  => 'No',
		],
		'button' => [
			'text' => 'Edit',
		],
		'date_format' => [
			'format'  => 'm/d/Y',
		    'tz'      => 'UTC',
		    'missing' => 'Not set',
		],
		'money' => [
			'decimals' => 2,
		    'symbol'   => '$',
		],
		'wrapper' => [
			'open'  => '<span>',
			'close' => '</span>',
		],
	]
];
