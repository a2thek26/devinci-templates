<?= Form::submit($options['label'], $options['attr']) ?>

<?php if(isset($options['buttons'])) : ?>
	<?php foreach($options['buttons'] as $button) : ?>
		<?php echo Html::link($button['attr']['href'], $button['label'], $button['attr']); ?>
	<?php endforeach; ?>
<?php endif; ?>
