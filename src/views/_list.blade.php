@extends('devinci-templates::template')

@section('content')
	@if(isset($name))
		<a class="btn btn-primary" href="#">New {{ studly_case($name)}}</a>
	@endif
	{!! $table !!}
@stop
