@extends('devinci-templates::template')

@section('content')
	@include('devinci-templates::_form-messages')
	{!! $form !!}
@stop
