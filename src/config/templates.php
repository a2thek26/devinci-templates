<?php

return [
	// template settings
    'namespace' => 'Templates',
    'uri'       => 'templates',
    'layout'    => 'master',
    'db_prefix' => 'template_',
    'separator' => ' / ',

    // field defaults
    'defaults' => [
	    'wrapper_class'       => 'form-group',
	    'wrapper_error_class' => 'has-error',
	    'label_class'         => 'control-label',
	    'field_class'         => 'form-control',
	    'help_block_class'    => 'help-block',
	    'error_class'         => 'text-danger',
	    'required_class'      => 'required'
    ],

    // field templates
    'form'       => 'devinci-fields::form',
    'text'       => 'devinci-fields::text',
    'textarea'   => 'devinci-fields::textarea',
    'button'     => 'devinci-fields::button',
    'radio'      => 'devinci-fields::radio',
    'checkbox'   => 'devinci-fields::checkbox',
    'select'     => 'devinci-fields::select',
    'choice'     => 'devinci-fields::choice',
    'repeated'   => 'devinci-fields::repeated',
    'child_form' => 'devinci-fields::child_form',
    'collection' => 'devinci-fields::collection',
    'static'     => 'devinci-fields::static',
    'boolean'    => 'devinci-fields::boolean',

    'custom_fields' => [
//        'toolbar' => 'Devinci\Templates\Fields\ToolbarType' // example
    ]
];
