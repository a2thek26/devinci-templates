<?php
namespace Devinci\Templates\Classes;

use App;
use Devinci\Templates\Traits\ConfigMaker;
use Request;
use View;

abstract class Controller extends ControllerBehavior
{
	use ConfigMaker;

	/**
	 * Configuration for form fields
	 *
	 * @var mixed
	 */
	protected $fieldConfig;

	/**
	 * Configuration for table
	 *
	 * @var mixed
	 */
	protected $tableConfig;

	/**
	 * The Form class
	 *
	 * @var
	 */
	protected $form = null;

	/**
	 * The FormBuilder instance
	 *
	 * @var
	 */
	protected $formBuilder;

	/**
	 * The TableBuilder instance
	 *
	 * @var
	 */
	protected $tableBuilder;

	/**
	 * The Model Class
	 *
	 * @var
	 */
	protected $model = null;

	/**
	 * Name for controller entity
	 *
	 * @var string
	 */
	protected $name;

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $rules = [];

	/**
	 * Construct.
	 */
	public function __construct()
	{
		parent::__construct();

		$this->name         = $this->getName();
		$this->form         = $this->getFormFromName();
	    $this->formBuilder  = App::make('laravel-form-builder');
		$this->tableBuilder = App::make('table');
		$this->tableConfig  = $this->makeConfig('columns', $this->name);
		$this->fieldConfig  = $this->makeConfig('fields', $this->name);
		$this->model        = $this->getModelFromName();
		$this->rules        = $this->getConfig('rules', $this->fieldConfig);
	}

	/**
	 * Must be set on Controller, defines the name for
	 * the set of classes that are represented by this template
	 *
	 * @return string
	 */
	abstract public function getName();

	/**
	 * Get list view
	 *
	 * @todo Pagination
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		$headline = $this->appendToHeadline($this->name);
		$records  = $this->getRecords();
		$table    = $this->makeTable($records);

		return $this->getListView(compact('table', 'headline'));
	}

	/**
	 * Make the create view
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		$form = $this->makeForm();

		return $this->getFormView(compact('form'));
	}

	/**
	 * Store a newly created model
	 *
	 * @todo Validation
	 * @return \Illuminate\View\View
	 */
	public function store()
	{
		$request = Request::instance();
		$this->validate($request, $this->rules);
		$this->model->create($request->input());

		return redirect(url($this->getPrefix().'index'));
	}

	/**
	 * Make the update view
	 *
	 * @todo Make extendable
	 * @param $id
	 * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$model = $this->getRecord($id);
		$form  = $this->makeForm($model);

		return $this->getFormView(compact('form'));
	}

	/**
	 * Update the record
	 *
	 * @todo Validation
	 * @param         $id
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function update($id)
	{
		$record = $this->model->find($id);
		$record->fill(Request::input())->save();

		return redirect(url($this->getPrefix().'edit', [$record]));
	}

	/**
	 * Get the "create" form attributes
	 *
	 * @return array
	 */
	protected function getCreateFormAttributes()
	{
		return [
			'method' => 'POST',
			'url'    => url($this->getPrefix().'store')
		];
	}

	/**
	 * Get the "edit" form attributes
	 *
	 * @param $record
	 * @return array
	 */
	protected function getEditFormAttributes($record)
	{
		return [
			'method' => 'PUT',
			'url'    => url($this->getPrefix().'update', [$record]),
			'model'  => $record
		];
	}

	/**
	 * Get a single record
	 *
	 * @param $id
	 * @return mixed
	 */
	protected function getRecord($id)
	{
		return $this->model->find($id);
	}

	/**
	 * Get the records for the table
	 *
	 * @return mixed
	 */
	protected function getRecords()
	{
		return $this->model->all();
	}

	/**
	 * Make the form
	 *
	 * @param null $model
	 * @return mixed
	 */
	protected function makeForm($model = null)
	{
		$formAttr = ($model !== null) ? $this->getEditFormAttributes($model) : $this->getCreateFormAttributes();

		return $this->formBuilder
			->create($this->getFormClassName(), $formAttr)
//			->add('save', 'submit', [
//				'attr' => ['class' => 'btn btn-primary']
//			])
			->add('toolbar', 'toolbar', [
				'attr'  => ['class' => 'btn btn-primary'],
			    'label' => 'Save',
			    'buttons' => [
				    [
					    'label' => 'Cancel',
				        'attr'  => [
					        'class' => 'btn btn-default',
				            'href'  => url($this->getPrefix()),
				        ],
			        ]
			    ]
			])
			->renderForm();
	}

	/**
	 * Get the prefix and resource name to generate a route prefix before the action
	 *
	 * @return string
	 */
	protected function getPrefix()
	{
		return $this->templatesUri.'/'.$this->name.'/';
	}

	/**
	 * Get the model based on name
	 *
	 * @return mixed
	 */
	protected function getModelFromName()
	{
		return $this->model ?: $this->model = App::make($this->getModelClassName());
	}

	/**
	 * Get the form based on name
	 *
	 * @return mixed
	 */
	protected function getFormFromName()
	{
		return $this->form ?: $this->form = App::make($this->getFormClassName());
	}

	/**
	 * Get the model class name with namespace
	 *
	 * @return string
	 */
	protected function getModelClassName()
	{
		return $this->getTemplatesAppNamespace().studly_case($this->name).'\\'.studly_case($this->name);
	}

	/**
	 * Get the namespace and class of the form
	 *
	 * @return string
	 */
	protected function getFormClassName()
	{
		return $this->getModelClassName().'Form';
	}

	/**
	 * Make the table
	 *
	 * @param $records
	 * @return mixed
	 */
	protected function makeTable($records)
	{
		return $this->tableBuilder
			->setColumns($this->getConfig('columns', $this->tableConfig))
			->setBefore($this->getConfig('before_widgets', $this->tableConfig))
			->setWidgets($this->getConfig('widgets', $this->tableConfig))
			->setAfter($this->getConfig('after_widgets', $this->tableConfig))
			->open([
				'id'    => 'table-'.$this->name,
				'class' => 'table table-striped'
			])
			->rows($records)
			->close()
			->render();
	}

}
