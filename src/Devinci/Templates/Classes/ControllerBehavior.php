<?php
namespace Devinci\Templates\Classes;

use Config;
use Illuminate\Console\AppNamespaceDetectorTrait;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as LaravelController;
use View;

abstract class ControllerBehavior extends LaravelController
{
	use DispatchesJobs, ValidatesRequests, AppNamespaceDetectorTrait;

	/**
	 * The action being called on this controller
	 *
	 * @var string
	 */
	public $action;

	/**
	 * Templates uri path
	 *
	 * @var string
	 */
	protected $templatesUri;

	/**
	 * Templates namespace
	 *
	 * @var mixed
	 */
	protected $templatesNamespace;

	/**
	 * The page title
	 *
	 * @var string
	 */
	protected $title = 'Templates';

	/**
	 * The page heading
	 *
	 * @var string
	 */
	protected $headline = 'Templates';

	/**
	 * The layout to use
	 *
	 * @var string
	 */
	protected $layout = 'master';

	/**
	 * The separation between headline sections
	 *
	 * @var string
	 */
	protected $separator =  ' / ';

	/**
	 * The default form view
	 *
	 * @var string
	 */
	protected $formDefault = '_form';

	/**
	 * The default list view
	 *
	 * @var string
	 */
	protected $listDefault = '_list';

	/**
	 * Constructor.
	 */
	public function __construct()
	{
		$this->templatesUri       = Config::get('templates.uri');
		$this->templatesNamespace = Config::get('templates.namespace', 'Templates');
		$this->layout             = Config::get('templates.layout', 'master');
		$this->separator          = Config::get('templates.separator', ' / ');
		$this->package            = 'devinci-templates';

		$this->setViewShares();
	}

	/**
	 * Set all the shares for the view
	 */
	protected function setViewShares()
	{
		View::share('layout', $this->layout);
		View::share('headline', $this->headline);
		View::share('title', $this->title);
	}

	/**
	 * Append to an existing headline
	 *
	 * @param $value
	 * @return string
	 */
	public function appendToHeadline($value)
	{
		return $this->prependTo($value, 'headline');
	}

	/**
	 * Prepend to an existing headline
	 *
	 * @param $value
	 * @return string
	 */
	public function prependToHeadline($value)
	{
		return $this->appendTo($value, 'headline');
	}

	/**
	 * Append to an existing title
	 *
	 * @param $value
	 * @return string
	 */
	public function appendToTitle($value)
	{
		return $this->prependTo($value, 'title');
	}

	/**
	 * Prepend to an existing title
	 *
	 * @param $value
	 * @return string
	 */
	public function prependToTitle($value)
	{
		return $this->appendTo($value, 'title');
	}

	/**
	 * Append to a class property value
	 *
	 * @param $value
	 * @param $property
	 * @return string
	 */
	protected function appendTo($value, $property)
	{
		return $this->{$property} = $value.$this->separator.$this->{$property};
	}

	/**
	 * Prepend to a class property value
	 *
	 * @param $value
	 * @param $property
	 * @return string
	 */
	protected function prependTo($value, $property)
	{
		return $this->{$property} = $this->{$property}.$this->separator.$value;
	}

	/**
	 * Get the namespace of template classes
	 *
	 * @return string
	 */
	protected function getTemplatesAppNamespace()
	{
		return $this->getAppNamespace().$this->templatesNamespace.'\\';
	}

	/**
	 * Get the prefix and resource name to generate a route prefix before the action
	 *
	 * @return string
	 */
	protected function getPrefix()
	{
		return $this->templatesUri.'/';
	}

	/**
	 * Check for form override or return default form view
	 *
	 * 1. First looks in resources/views/<prefix>/<name> for generic: _form
	 * 2. Then looks in the same directory for form context: _form-create or _form-edit
	 * 3. Otherwise falls back to default
	 *
	 * @param $data
	 * @return \Illuminate\View\View
	 */
	protected function getFormView($data)
	{
		$default    = $this->getDefaultView('form');
		$form       = $this->getPrefix().$this->formDefault;
		$formAction = $form.'-'.$this->action;

		if (View::exists($form)) {
			$view = $form;
		} else if (View::exists($formAction)) {
			$view = $formAction;
		} else {
			$view = $default;
		}

		return View::make($view, $data);
	}

	/**
	 * Check for list view override, otherwise default it
	 *
	 * @param $data
	 * @return \Illuminate\View\View
	 */
	protected function getListView($data)
	{
		$view = $this->getDefaultView('list');
		$list = $this->getPrefix().$this->listDefault;

		if(View::exists($list)) {
			$view = $list;
		}

		return View::make($view, $data);
	}

	/**
	 * Get default view based on context
	 *
	 * @param $context
	 * @return string
	 */
	protected function getDefaultView($context)
	{
		return $this->package.'::'.$this->{$context.'Default'};
	}

}
