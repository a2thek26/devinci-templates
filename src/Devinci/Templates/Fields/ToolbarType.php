<?php
namespace Devinci\Templates\Fields;

use Kris\LaravelFormBuilder\Fields\FormField;

class ToolbarType extends FormField
{

	/**
	 * Get the template, can be config variable or view path
	 *
	 * @return string
	 */
	protected function getTemplate()
	{
		return 'devinci-fields::toolbar';
	}

}
