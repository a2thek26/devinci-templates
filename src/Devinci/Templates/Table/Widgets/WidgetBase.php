<?php
namespace Devinci\Templates\Table\Widgets;

use Illuminate\Html\HtmlBuilder;
use Lang;

abstract class WidgetBase
{
	/**
	 * @var HtmlBuilder
	 */
	protected $html;

	/**
	 * @param HtmlBuilder $html
	 */
	public function __construct(HtmlBuilder $html)
	{
		$this->html = $html;
	}

	/**
	 * Render the table widget
	 *
	 * @param $value
	 * @param $record
	 * @param $config
	 * @return mixed
	 */
	abstract public function render($value, $record, $config);

	/**
	 * Helper to check if a key exists on a config and return default if not
	 *
	 * @param        $key
	 * @param        $config
	 * @param string $default
	 * @return string
	 */
	public function getValue($key, $config, $default = '')
	{
		$default = ($default !== '') ? $default: $this->getDefaultFromLanguage($key);

		return isset($config[$key]) ? $config[$key] : $default;
	}

	/**
	 * Get the default value from the language file
	 *
	 * @param $key
	 * @return string
	 */
	protected function getDefaultFromLanguage($key)
	{
		return Lang::get($this->parseLanguagePath($key));
	}

	/**
	 * Convert namespace path to language path
	 *
	 * @example
	 * Devinci\Templates\Table\Widgets\Bold to table.widget.bold
	 *
	 * @param $key
	 * @return string
	 */
	protected function parseLanguagePath($key)
	{
		$pieces = explode('\\', get_class($this));
		array_push($pieces, $key);
		$pieces = array_slice($pieces, 2);

//		dd($pieces);

		$path = [];
		foreach($pieces as $piece) {
			$path[] = snake_case(str_singular($piece));
		}

		return implode('.', $path);
	}
}
