<?php
namespace Devinci\Templates\Table\Widgets;

class Wrapper extends WidgetBase
{

	/**
	 * Wrap a table value with some markup
	 *
	 * Possible config values:
	 * - open (string)  The opening wrapper tag. default: '<span>'
	 * - close (string) The closing wrapper tag. default: '</span>'
	 *
	 * @param $value
	 * @param $record
	 * @param $config
	 * @return mixed
	 */
	public function render($value, $record, $config)
	{
		$open  = $this->getValue('open', $config);
		$close = $this->getValue('close', $config);

		return $open.$value.$close;
	}
}
