<?php namespace Devinci\Templates\Table\Widgets;

use Carbon\Carbon;

class DateFormat extends WidgetBase
{

	/**
	 * Format a date value
	 *
	 * Possible config values:
	 * - format  (string) The date format. default: 'm/d/Y'
	 * - tz      (string) The timezone to render the date in. default: 'UTC'
	 * - missing (string) The text to display if value is not a valid date. default: 'Not set'
	 *
	 * @param       $value
	 * @param       $record
	 * @param array $config
	 * @return string
	 */
	public function render($value, $record, $config = [])
	{
		$format = $this->getValue('format', $config);
		$tz     = $this->getValue('tz', $config);
		$text   = $this->getValue('missing', $config);

		return ( $value == '0000-00-00' || $value == '0000-00-00 00:00:00' || ! is_null($value) )
			? Carbon::parse($value)->tz($tz)->format($format)
			: '<span class="text-muted"><i>'.$text.'</i></span>';

	}

}
