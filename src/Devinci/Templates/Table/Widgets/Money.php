<?php namespace Devinci\Templates\Table\Widgets;

class Money extends WidgetBase
{

	/**
	 * Transform a number into money
	 *
	 * Possible config values:
	 * - symbol (string) The monetary symbol to display. default: '$'
	 * - decimals (int)  The amount of decimal places to display. default: 2
	 *
	 * @param       $value
	 * @param       $record
	 * @param array $config
	 * @return string
	 */
	public function render($value, $record, $config = [])
	{
		$symbol   = $this->getValue('symbol', $config);
		$decimals = $this->getValue('decimals', $config);

		return $symbol.number_format($value, $decimals);
	}
}
