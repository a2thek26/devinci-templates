<?php namespace Devinci\Templates\Table\Widgets;

class Button extends WidgetBase {

	/**
	 * @var array
	 */
	protected $classes = 'btn btn-xs btn-default';

	/**
	 * Build a button into a table row
	 *
	 * Possible config values:
	 * - text       (string) The text to display on the button. default: 'Edit'
	 * - route      (string) The url the button should link to. default: null
	 * - key        (string) The record key to use when applying route parameters. default: 'id'
	 * - parameters (array)  The route parameters. default: array()
	 * - replace    (string) The record key to use when replacing pieces of the url. default: null
	 * - attributes (array)  Additional attributes to place on the button. default: ['class' => 'btn btn-default']
	 * - icon       (string) The font awesome icon name. default: null
	 * - placement  (string) Where the icon should be place in relation to the text. default: 'before'
	 * - condition  (array)  An array with 'key', 'condition', and 'value' as the first three values
	 *
	 * @param $record
	 * @param $config
	 * @return string
	 */
	public function render($val, $record, $config)
	{
		$text       = $this->getValue('text', $config);
		$route      = $this->getValue('route', $config, null);
		$key        = $this->getValue('key', $config, null);
		$params     = $this->getValue('parameters', $config, []);
		$replace    = $this->getValue('replace', $config, null);
		$attributes = $this->getValue('attributes', $config, ['class' => $this->classes]);
		$icon       = $this->getValue('icon', $config, null);
		$placement  = $this->getValue('placement', $config, 'before');
		$condition  = $this->getValue('condition', $config, true);

		$button     = '';

		if($this->evalCondition($record, $condition)) {
			$text       = $this->buildIcon($icon, $placement, $text);
			$route      = $this->buildRoute($record, $route, $params, $key, $replace);
			$attributes = $this->buildAttributes($record, $attributes);
			$button     = $this->buildButton($route, $attributes, $text);
		}

		return $button;
	}

	/**
	 * Evaluate the condition
	 *
	 * @param $record
	 * @param $condition
	 * @return bool|float
	 */
	protected function evalCondition($record, $condition)
	{
		if(is_bool($condition)) return $condition;

		if(is_array($condition)) {
			$key      = $record->{$condition[0]};
			$operator = $condition[1];
			$value    = $condition[2];

			return $this->evaluate($operator, $key, $value);
		}

		return true;
	}

	/**
	 * Return the result of the condition
	 *
	 * @param $operator
	 * @param $key
	 * @param $value
	 * @return bool|float
	 */
	protected function evaluate($operator, $key, $value)
	{
		switch($operator) {
			case '===' : return $key === $value;
			case '=='  : return $key == $value;
			case '!='  : return $key != $value;
			case '+'   : return $key + $value;
			case '-'   : return $key - $value;
			case '*'   : return $key * $value;
			case '/'   : return $key / $value;
			default    : return true;
		}
	}

	/**
	 * Build the icon with before or after text placement
	 *
	 * @param $icon
	 * @param $placement
	 * @param $text
	 * @return string
	 */
	protected function buildIcon($icon, $placement, $text)
	{
		if( ! is_null($icon)){
			$icon = '<i class="fa ' . $icon . '"></i>';
			$text = ($placement === 'before') ? $icon.' '.$text : $text.' '.$icon;
		}

		return $text;
	}

	/**
	 * @param $record
	 * @param $route
	 * @param $params
	 * @param $key
	 * @param $replace
	 * @return string
	 */
	protected function buildRoute($record, $route, $params, $key, $replace)
	{
		if( ! is_null($route)) {
			if($replace !== null) {
				$route = str_replace('{'.$replace.'}', $record->{$replace}, $route);
			}
			if($key !== null) {
				$params = array_merge($params, [$record->{$key}]);
			}
			$route  = url($route, $params);
		} else {
			$route = '#';
		}

		return $route;
	}

	/**
	 * Build button with data attributes with values from the record
	 *
	 * If the key is present on the 'data' array
	 * it will create a new attribute with 'data-' as a prefix
	 * and will return the value of the record with the same property name
	 *
	 * @example
	 * $config = [
	 *     'data' => ['name', 'address'],
	 * ];
	 *
	 * Would be converted to this:
	 * $attributes['data-name'] = $record->name;
	 * $attributes['data-address'] = $record->address;
	 *
	 * @param $record
	 * @param $attributes
	 * @return mixed
	 */
	protected function buildAttributes($record, $attributes)
	{
		if(array_key_exists('data', $attributes)){
			foreach($attributes['data'] as $key) {
				$attributes['data-' . $key] = $record->{$key};
			}
		}
		array_pull($attributes, 'data');

		return $this->html->attributes($attributes);
	}

	/**
	 * Build the button
	 *
	 * @param $route
	 * @param $attributes
	 * @param $text
	 * @return string
	 */
	protected function buildButton($route, $attributes, $text)
	{
		return '<a href="'.$route.'"'.$attributes.'>'.$text.'</a>';
	}
}
