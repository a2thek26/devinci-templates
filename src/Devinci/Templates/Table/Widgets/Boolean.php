<?php namespace Devinci\Templates\Table\Widgets;

class Boolean extends WidgetBase {

	/**
	 * Turn a boolean value into a reader-friendly value
	 *
	 * Possible config values:
	 * - yes (string) The text to display if the boolean is true. default: 'Yes'
	 * - no (string) The text to display if the boolean is false. default: 'No'
	 *
	 * @param       $value
	 * @param       $record
	 * @param array $config
	 * @return string
	 */
	public function render($value, $record, $config = [])
	{
		$yes = $this->getValue('yes', $config);
		$no  = $this->getValue('no', $config);

		return ($value == 1) ? $yes : $no;
	}
}
