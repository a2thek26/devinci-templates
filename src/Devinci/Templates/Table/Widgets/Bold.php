<?php namespace Devinci\Templates\Table\Widgets;

use App;

class Bold extends WidgetBase {

	/**
	 * Bold a value
	 *
	 * Possible config values:
	 * None
	 *
	 * @param       $value
	 * @param       $record
	 * @param array $config
	 * @return string
	 */
	public function render($value, $record, $config = [])
	{
		$config = [
			'open' => '<strong>',
		    'close' => '</strong>'
		];

		return App::make('table.widget.wrapper')->render($value, $record, $config);
	}
}
