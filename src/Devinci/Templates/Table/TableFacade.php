<?php namespace Devinci\Templates\Table;

use Illuminate\Support\Facades\Facade;

class TableFacade extends Facade {
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor() { return 'table'; }
}
