<?php namespace Devinci\Templates\Table;

use Illuminate\Support\ServiceProvider;

class TableServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 * @return void
	 */
	public function register()
	{
		$this->registerTableBuilder();
		$this->registerWidgets();
	}

	/**
	 * Register the TableBuilder class
	 */
	protected function registerTableBuilder()
	{
		$this->app->bind('table', function ($app) {
			return new TableBuilder($app['html']);
		});
	}

	/**
	 * Register table widgets
	 */
	protected function registerWidgets()
	{
		$this->app->bind('table.widget.bold', 'Devinci\Templates\Table\Widgets\Bold');
		$this->app->bind('table.widget.boolean', 'Devinci\Templates\Table\Widgets\Boolean');
		$this->app->bind('table.widget.button', 'Devinci\Templates\Table\Widgets\Button');
		$this->app->bind('table.widget.date', 'Devinci\Templates\Table\Widgets\DateFormat');
		$this->app->bind('table.widget.money', 'Devinci\Templates\Table\Widgets\Money');
		$this->app->bind('table.widget.wrapper', 'Devinci\Templates\Table\Widgets\Wrapper');
	}

	/**
	 * Get the services provided by the provider.
	 * @return array
	 */
	public function provides()
	{
		return ['table'];
	}

}
