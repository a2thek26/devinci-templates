<?php namespace Devinci\Templates\Table;

use Illuminate\Html\HtmlBuilder as Html;

class TableBuilder {

	/**
	 * @var Html
	 */
	protected $html;

	/**
	 * Data columns
	 * @example ['name', 'Hire Date' => 'start_date', 'group.name'];
	 * // would output: Name, Hire Date, Group Name
	 */
	protected $columns = [];

	/**
	 * Column filters
	 * @var array
	 */
	protected $filters = [];

	/**
	 * Data widgets
	 * @var array
	 */
	protected $widgets = [];

	/**
	 * Before row widgets
	 * @var array
	 */
	protected $before = [];

	/**
	 * After row widgets
	 * @var array
	 */
	protected $after = [];

	/**
	 * Stack of table output
	 * @var array
	 */
	protected $stack = [];

	/**
	 * @param Html $html
	 */
	public function __construct(Html $html)
	{
		$this->html = $html;
	}

	/**
	 * Set config for table
	 * @param array $config
	 * @return $this
	 */
	public function setConfig(array $config = [])
	{
		$this->columns = (isset($config['columns'])) ? $config['columns'] : $this->columns;
		$this->filters = (isset($config['filters'])) ? $config['filters'] : $this->filters;
		$this->before  = (isset($config['before'])) ? $config['before'] : $this->before;
		$this->widgets = (isset($config['widgets'])) ? $config['widgets'] : $this->widgets;
		$this->after   = (isset($config['after'])) ? $config['after'] : $this->after;

		return $this;
	}

	/**
	 * Set config for columns
	 * @param array $config
	 * @return $this
	 */
	public function setColumns(array $config = [])
	{
		$this->columns = $config;

		return $this;
	}

	/**
	 * Set config for columns
	 * @param array $config
	 * @return $this
	 */
	public function setFilters(array $config = [])
	{
		$this->filters = $config;

		return $this;
	}

	/**
	 * Set before row widgets
	 * @param array $config
	 * @return $this
	 */
	public function setBefore(array $config = [])
	{
		$this->before = $config;

		return $this;
	}

	/**
	 * Set field widgets
	 * @param array $config
	 * @return $this
	 */
	public function setWidgets(array $config = [])
	{
		$this->widgets = $config;

		return $this;
	}

	/**
	 * Set after row widgets
	 * @param array $config
	 * @return $this
	 */
	public function setAfter(array $config = [])
	{
		$this->after = $config;

		return $this;
	}

	/**
	 * Open up the table
	 * @param array $attributes
	 * @return string
	 */
	public function open($attributes = [])
	{
		$attributes = $this->html->attributes($attributes);

		$this->stack[] = '<table ' . $attributes . '>';

		return $this;
	}

	/**
	 * Output the table rows
	 * @param $records
	 * @return string
	 */
	public function rows($records)
	{
		$heading = '<thead>';
		$heading .= $this->makeColumnHeadings();
		if( ! empty($this->filters)) {
			$heading .= $this->makeColumnFilters();
		}
		$heading .= '</thead>';


		$rows = '';

		foreach($records as $record) {
			$rows .= $this->makeRow($record);
		}

		$this->stack[] = $heading . $rows;

		return $this;
	}

	/**
	 * @return string Close up the table
	 */
	public function close()
	{
		$this->stack[] = '</table>';

		return $this;
	}

	/**
	 * Build column headings
	 * @return string
	 */
	protected function makeColumnHeadings()
	{
		$heading = '<tr class="table-headings">';

		if(count($this->before))
			$heading .= $this->renderSpacers('before', 'th');

		foreach($this->columns as $label => $field) {
			$heading .= "<th class='col-{$field}'>{$this->makeColumnHeading($label, $field)}</th>";
		}

		if(count($this->after))
			$heading .= $this->renderSpacers('after', 'th');

		$heading .= '</tr>';

		return $heading;
	}

	/**
	 * Create a row for filtering
	 * @return string
	 */
	protected function makeColumnFilters()
	{
		$heading = '<tr class="table-filters">';

		if(count($this->before))
			$heading .= $this->renderSpacers('before', 'td');

		foreach($this->columns as $label => $field) {
			$heading .= "<td class='col-filter-{$field}'>{$this->makeColumnFilter($field)}</td>";
		}

		if(count($this->after))
			$heading .= $this->renderSpacers('after', 'td');

		$heading .= '</tr>';

		return $heading;
	}

	/**
	 * Make single heading value
	 * @param $label
	 * @param $field
	 * @return string
	 */
	protected function makeColumnHeading($label, $field)
	{
		return (is_string($label)) ? $label : $this->humanizeText($field);
	}

	/**
	 * Render leading and closing columns for extended widgets
	 * @param $scope
	 * @param string $markup
	 * @return string
	 */
	protected function renderSpacers($scope, $markup = 'th')
	{
		$heading = '';
		foreach($this->{$scope} as $col) {
			$heading .= "<{$markup} class=\"no-sort\">&nbsp;</{$markup}>";
		}

		return $heading;
	}


	/**
	 * Make column filters
	 * @param $field
	 * @return string
	 */
	protected function makeColumnFilter($field)
	{
		if(array_key_exists($field, $this->filters)) {
			$filter = $this->filters[$field];

			switch($filter['type']) {
				case 'select' :
					$list = $filter['data'];

					$cleaned = [];
					foreach($list as $key => $val) {
						$key = strip_tags($key);
						$val = strip_tags($val);
						$cleaned[$key] = $val;
					}

					return app('form')->select($field, $cleaned, null, [
						'class' => 'form-control select2'
					]);

				case 'search' :

					return app('form')->text($field, null, [
						'placeholder' => trans('global.search', 'Search'),
						'class'       => 'form-control search'
					]);
			}

			return $filter['type'];
		}

		return '&nbsp;';

	}

	/**
	 * Build column values
	 * @param $record
	 * @return string
	 */
	protected function makeRow($record)
	{
		$rows = '<tr>';

		if(count($this->before))
			$rows .= $this->renderSide('before', $record);

		foreach($this->columns as $column) {
			$rows .= "<td>{$this->determineColumnOutput($record, $column)}</td>";
		}

		if(count($this->after))
			$rows .= $this->renderSide('after', $record);

		$rows .= '</tr>';

		return $rows;
	}

	/**
	 * Render widgets in either side of row
	 *
	 * @param $scope
	 * @param $record
	 * @return string
	 */
	protected function renderSide($scope, $record)
	{
		$output = [];

		foreach($this->{$scope} as $widget => $config) {
			$widget = (array_key_exists('widget', $config)) ? $config['widget'] : $widget;
			$output[] = '<td width="1">' . app($widget)->render(null, $record, $config) . '</td>';
		}

		return implode('', $output);
	}

	/**
	 * Checks for object-style dot notation
	 *
	 * @example Turns `user.group.name` to `$user->group->name` to get the related value
	 * @param $record
	 * @param $column
	 * @return mixed
	 */
	protected function determineColumnOutput($record, $column)
	{
		if(is_null($column) || trim($column) == '') return $record;

		$value = $record;
		foreach(explode('.', $column) as $segment) {
			$value = $value->{$segment};
		}

		if(array_key_exists($column, $this->widgets)) {
			$widget = $this->widgets[$column];
			$config = [];

			if(is_array($this->widgets[$column])) {
				$widget = $this->widgets[$column]['widget'];
				$config = $this->widgets[$column]['config'];
			}

			return app($widget)->render($value, $record, $config);
		}

		return strip_tags($value);
	}

	/**
	 * Output chain
	 * @return string
	 */
	public function render()
	{
		return implode('', $this->stack);
	}

	/**
	 * @param $str
	 * @return string
	 */
	protected function humanizeText($str)
	{
		$dotName = snake_case($str);

		return ucwords(str_replace(['-', '_', '.'], ' ', $dotName));
	}
}
