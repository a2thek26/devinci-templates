<?php
namespace Devinci\Templates\Traits;

use Config;
use File;
use League\Flysystem\FileNotFoundException;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Yaml;

trait ConfigMaker
{

	/**
	 * Make a config array
	 *
	 * @param $file
	 * @param $template
	 * @return mixed
	 * @throws FileNotFoundException
	 */
	public function makeConfig($file, $template)
	{
		$file = starts_with($file, 'config_') ? $file : 'config_'.$file;
		$path = $this->getConfigPath($file, $template);

		if(! File::exists($path)) {
			throw new FileNotFoundException($path, 404);
		}

//		$yaml   = new Parser;
//		$config = $yaml->parse(File::get($path));
		$config = Yaml::parse(File::get($path));

		return $config;
	}

	/**
	 * Get the full path based on templates config, file, and entity (directory)
	 *
	 * @param $file
	 * @param $template
	 * @return string
	 */
	public function getConfigPath($file, $template)
	{
		return app_path(Config::get('templates.namespace').'/'.$template.'/'.$file.'.yaml');
	}

	/**
	 * Call config helper method if the method exists in the form of get{KeyName}Config,
	 * otherwise check if key exists on config and return the array,
	 * or just return an empty array
	 *
	 * Since any class can use this trait, the configuration can be modified and extended by creating
	 * a method on the "traited" class. This can allow the developer to easily add simple syntax to
	 * the configuration and parse it however they want in the code.
	 *
	 * @see $this->getColumnsConfig
	 * @param $key
	 * @param $config
	 * @return array
	 */
	public function getConfig($key, $config)
	{
		$method = 'get'.studly_case($key).'Config';

		if(method_exists($this, $method)) {
			return $this->{$method}($config);
		}

		if($this->configHasKey($key, $config)) {
			return (array) $config[$key];
		}

		return [];
	}

	/**
	 * Parse the columns from the config
	 *
	 * @param $config
	 * @return array
	 */
	public function getColumnsConfig($config)
	{
		if($this->configHasKey('columns', $config)) {
			$columns = [];
			foreach($config['columns'] as $key => $val) {
				if(is_array($val) && array_key_exists('label', $val)) {
					$columns[$val['label']] = $key;
				} else {
					$columns[] = $key;
				}
			}

			return $columns;
		}

		return ['ID' => 'id'];
	}

	/**
	 * Quick check for key on config
	 *
	 * @param $key
	 * @param $config
	 * @return bool
	 */
	protected function configHasKey($key, $config)
	{
		return (is_array($config) && array_key_exists($key, $config));
	}
}
