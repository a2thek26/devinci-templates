<?php namespace Devinci\Templates\Console;

use Config;

class TemplateSpawn extends SpawnGenerator
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'spawn:template {name} {--fields=}';

	/**
	 * @var string
	 */
	protected $description = 'Spawn the assets for a new template.';

	/**
	 * List of configs to be generated
	 *
	 * @var array
	 */
	protected $configs = ['fields', 'columns'];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    $name = $this->argument('name');

	    // generate template classes
	    foreach($this->classes as $type) {
		    $command = snake_case($type);
		    $this->call('spawn:'.$command, [
			    'name'     => ($type === 'Model') ? $name : $name.$type,
			    'template' => $name
		    ]);
	    }

	    // generate configs
	    foreach($this->configs as $config) {
			$this->call('spawn:'.$config, [
				'template' => $name
			]);
	    }

	    // generate migration
	    $this->call('make:migration:schema', [
		    'name'     => 'create_'. $this->getTableName() .'_table',
	        '--schema' => $this->option('fields') ?: '',
	        '--model'  => false,
	    ]);
    }

}
