<?php namespace Devinci\Templates\Console;

class ControllerSpawn extends SpawnGenerator
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'spawn:controller {name} {template} {--model=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Spawn a new Devinci controller.';

	/**
	 * @var string
	 */
	protected $type = 'Controller';

	/**
	 * Build the class with the given name.
	 *
	 * @param  string  $name
	 * @return string
	 */
	protected function buildClass($name)
	{
		$stub = $this->files->get($this->getStub());

		return $this
			->replaceNamespace($stub, $name)
			->replaceModelAlias($stub)
			->replaceClass($stub, $name);
	}

	/**
	 * Replace the model alias in the controller
	 *
	 * @param $stub
	 * @return $this
	 */
	protected function replaceModelAlias(&$stub)
	{
		$alias = $this->option('model') ?: $this->getRawName();
		$stub  = str_replace('DummyModelAlias', camel_case($alias), $stub);

		return $this;
	}

}
