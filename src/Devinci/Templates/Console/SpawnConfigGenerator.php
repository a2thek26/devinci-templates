<?php
namespace Devinci\Templates\Console;

abstract class SpawnConfigGenerator extends SpawnGenerator
{
	/**
	 * Get the destination config path.
	 *
	 * @param  string  $name
	 * @return string
	 */
	protected function getPath($name)
	{
		$name = str_replace($this->laravel->getNamespace(), '', $name);

		$name = explode('\\', $name);
		array_pop($name);
		array_push($name, $this->type);
		$name = implode('\\', $name);

		return $this->laravel['path'].'/'.str_replace('\\', '/', $name).'.yaml';
	}

	/**
	 * @return string
	 */
	protected function getNameInput()
	{
		return $this->argument('template');
	}
}
