<?php namespace Devinci\Templates\Console;

class ColumnConfigSpawn extends SpawnConfigGenerator
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'spawn:columns {template}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a new column configuration.';

	/**
	 * @var string
	 */
	protected $type = 'config_columns';
}
