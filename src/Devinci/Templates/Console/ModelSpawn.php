<?php
namespace Devinci\Templates\Console;

use Config;

class ModelSpawn extends SpawnGenerator
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'spawn:model {name} {template} {--migration}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Spawn a new Devinci model.';

	/**
	 * @var string
	 */
	protected $type = 'Model';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    if($this->option('migration')) {

		    $tableName = $this->getTableName();

		    $this->call('make:migration', [
			    'name'     => 'create_'.$tableName.'_table',
			    '--create' => $tableName
		    ]);
	    }

        parent::fire();
    }

	/**
	 * Build the class with the given name.
	 *
	 * @param  string  $name
	 * @return string
	 */
	protected function buildClass($name)
	{
		$stub = $this->files->get($this->getStub());

		return $this
			->replaceNamespace($stub, $name)
			->replaceTableName($stub)
			->replaceClass($stub, $name);
	}

	/**
	 * Replace the table name in the model
	 *
	 * @param $stub
	 * @return $this
	 */
	protected function replaceTableName(&$stub)
	{
		$table = $this->getTableName();
		$stub  = str_replace('DummyTableName', $table, $stub);

		return $this;
	}

}
