<?php
namespace Devinci\Templates\Console;

use Config;
use Illuminate\Console\GeneratorCommand;

abstract class SpawnGenerator extends GeneratorCommand
{

	/**
	 * Generated class types
	 *
	 * @var array
	 */
	protected $classes = ['Controller', 'Form', 'Model'];

	/**
	 * Generated config types
	 *
	 * @var array
	 */
	protected $configs = ['config_fields', 'config_columns'];

	/**
	 * Handle the command
	 */
	public function handle()
	{
		parent::fire();
	}

	/**
	 * Get namespace for class
	 *
	 * @param string $root
	 * @return string
	 */
	public function getDefaultNamespace($root)
	{
		$namespace = $root.'\\'.Config::get('templates.namespace', 'Templates');

		return ($this->argument('template')) ? $namespace.'\\'.ucfirst($this->argument('template')) : $namespace;
	}

	/**
	 * Get the stub file for the generator.
	 *
	 * @return string
	 */
	protected function getStub()
	{
		return __DIR__.'/stubs/'.snake_case($this->type).'.stub';
	}

	/**
	 * Add a class type to check against for the raw name
	 *
	 * @param $name
	 */
	protected function addClassType($name) {
		$this->classes[] = $name;
	}

	/**
	 * Get the raw name without class type suffix
	 *
	 * @return string
	 */
	protected function getRawName()
	{
		$name = $this->argument('name');

		return (ends_with($name, $this->classes)) ? str_replace($this->classes, '', $name) : $name;
	}

	/**
	 * Get the table name
	 *
	 * @return string
	 */
	protected function getTableName()
	{
		return Config::get('templates.db_prefix').snake_case(str_plural($this->argument('name')));
	}
}
