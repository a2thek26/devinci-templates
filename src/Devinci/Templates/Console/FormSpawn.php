<?php
namespace Devinci\Templates\Console;

class FormSpawn extends SpawnGenerator
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'spawn:form {name} {template}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Spawn a new Devinci form.';

	/**
	 * @var string
	 */
	protected $type = 'Form';

}
