<?php
namespace Devinci\Templates\Form;

use App;
use Devinci\Templates\Traits\ConfigMaker;
use Kris\LaravelFormBuilder\Form;

abstract class BaseForm extends Form
{
	use ConfigMaker;

	/**
	 * Form alias
	 *
	 * @var string
	 */
	protected $alias = null;

	/**
	 * Entire field configuration
	 *
	 * @var array
	 */
	public $config;

	/**
	 * Configuration for fields
	 *
	 * @var array
	 */
	public $fieldConfig;

	/**
	 * Construct
	 */
	public function __construct()
	{
	    $this->alias       = $this->getFormAlias();
		$this->config      = $this->makeConfig('fields', $this->alias);
		$this->fieldConfig = $this->getConfig('fields', $this->config);
		$this->rulesConfig = $this->getConfig('rules', $this->config);
		$this->formHelper  = App::make('laravel-form-helper');
		$this->formBuilder = App::make('laravel-form-builder');
	}

	/**
	 * Get the form alias
	 * @return string
	 */
	protected function getFormAlias()
	{
		if(! is_null($this->alias)) return $this->alias;

		$className = class_basename($this);
		if(ends_with($className, 'Form')) {
			$className = substr($className, 0, -4);
		}

		return $this->alias = snake_case($className);
	}

	/**
	 * Build out the form from the field config
	 */
	public function buildForm()
	{
		$this->addCustomField('toolbar', 'Devinci\Templates\Fields\ToolbarType');

		foreach($this->fieldConfig as $name => $options) {
			$type = array_pull($options, 'type') ?: 'text';
			$this->add($name, $type, $options);
		}
	}

}
