<?php namespace Devinci\Templates\Controllers;

use App;
use Devinci\Templates\Classes\ControllerBehavior;

class TemplateController extends ControllerBehavior
{

	/**
	 * Entry point for all template controllers
	 *
	 * @todo Put checks in for segments and existence of controller in filesystem
	 * @param null $uri
	 * @return mixed
	 */
	public function run($uri = null)
	{
		/**
		 * Parse the uri to get controller, action, and params
		 */
		$segments   = explode('/', $uri);
		$controller = isset($segments[0]) ? $segments[0] : 'index';
		$action     = isset($segments[1]) ? $segments[1] : 'index';
		$params     = array_slice($segments, 2);

		/**
		 * Create the controller
		 */
		$controller    = $this->getControllerName($controller);
		$controllerObj = App::make($controller);
		$controllerObj->action = $action;

		return $controllerObj->callAction($action, $params);
	}

	/**
	 * Get the controller name
	 *
	 * @param $controller
	 * @return string
	 */
	protected function getControllerName($controller)
	{
		$controller = studly_case($controller);

		return $this->getTemplatesAppNamespace().$controller.'\\'.$controller.'Controller';
	}

}
