<?php
namespace Devinci\Templates\Controllers;

use App;
use Devinci\Templates\Classes\ControllerBehavior;
use File;

class IndexController extends ControllerBehavior
{

	/**
	 * Output all the templates in a list view
	 */
	public function index()
	{
		$templates   = [];
		$directories = File::directories(app_path($this->templatesNamespace));

		foreach($directories as $directory) {
			$directory   = explode('/', $directory);
			$type        = array_pop($directory);
			$templates[] = (object)[
				'page'  => $type,
				'alias' => strtolower($type),
			];
		}

		$table = App::make('table')
			->setColumns(['Page Type' => 'page'])
			->setBefore([
				 [
					 'widget'  => 'table.widget.button',
					 'text'    => 'New',
					 'route'   => 'templates/{alias}/create',
					 'replace' => 'alias'
				 ],
				 [
					 'widget'  => 'table.widget.button',
					 'text'    => 'List',
					 'route'   => 'templates/{alias}',
					 'replace' => 'alias'
				 ]
			])
			->open(['class' => 'table table-striped'])
			->rows($templates)
			->close()
			->render();

		return $this->getListView(compact('table'));

	}
}
