<?php
namespace Devinci\Templates;

use App;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Html\FormBuilder as LaravelForm;
use Illuminate\Html\HtmlBuilder;
use Illuminate\Support\ServiceProvider;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormHelper;

class TemplateServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
	    $this->loadViewsFrom(__DIR__ . '/../../views', 'devinci-templates');
	    $this->loadViewsFrom(__DIR__ . '/../../views/fields', 'devinci-fields');

	    $this->loadTranslationsFrom(__DIR__.'/../../lang', 'table');

	    $this->publishes([
	        __DIR__.'/../../views' => base_path('resources/views/vendor/devinci-templates')
	    ], 'views');

	    $this->publishes([
		    __DIR__.'/../../config/templates.php' => config_path('templates.php'),
	    ], 'config');

	    include __DIR__ . '/../../routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
	    if ($this->app->environment() == 'local') {
		    $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
	    }

	    $this->mergeConfigFrom(__DIR__ . '/../../config/templates.php', 'templates');
	    $this->registerHtmlIfNeeded();
	    $this->registerFormIfHeeded();
	    $this->registerFormBuilder();
	    $this->registerTableBuilder();
	    $this->registerConsole();
    }

	/**
	 * Register FormBuilder dependency
	 */
	protected function registerFormBuilder()
	{
		$this->app->bindShared('laravel-form-helper', function ($app) {
			$configuration = $app['config']->get('templates');

			return new FormHelper($app['view'], $app['request'], $configuration);
		});

		$this->app->alias('laravel-form-helper', 'Kris\LaravelFormBuilder\FormHelper');

		$this->app->bindShared('laravel-form-builder', function ($app) {
			return new FormBuilder($app, $app['laravel-form-helper']);
		});

		$this->app->booting(function() {
			AliasLoader::getInstance()->alias('FormBuilder', 'Kris\LaravelFormBuilder\Facades\FormBuilder');
		});
	}

	/**
	 * Register TableBuilder
	 */
	protected function registerTableBuilder()
	{
		$this->app->register('Devinci\Templates\Table\TableServiceProvider');
		$this->app->booting(function() {
			AliasLoader::getInstance()->alias('TableBuilder', 'Devinci\Templates\Table\TableFacade');
		});
	}

	/**
	 * Register the console commands
	 */
	protected function registerConsole()
	{
		$this->commands([
			'Devinci\Templates\Console\ModelSpawn',
			'Devinci\Templates\Console\ControllerSpawn',
			'Devinci\Templates\Console\FormSpawn',
			'Devinci\Templates\Console\TemplateSpawn',
			'Devinci\Templates\Console\FieldConfigSpawn',
			'Devinci\Templates\Console\ColumnConfigSpawn',
		]);
	}

	/**
	 * Add Laravel Form to container if not already set
	 */
	private function registerFormIfHeeded()
	{
		if (!$this->app->offsetExists('form')) {

			$this->app->bindShared('form', function($app) {
				$form = new LaravelForm($app['html'], $app['url'], $app['session.store']->getToken());

				return $form->setSessionStore($app['session.store']);
			});

			if (! $this->aliasExists('Form')) {
				AliasLoader::getInstance()->alias('Form', 'Illuminate\Html\FormFacade');
			}
		}
	}

	/**
	 * Add Laravel Html to container if not already set
	 */
	private function registerHtmlIfNeeded()
	{
		if (!$this->app->offsetExists('html')) {

			$this->app->bindShared('html', function($app) {
				return new HtmlBuilder($app['url']);
			});

			if (! $this->aliasExists('Html')) {
				AliasLoader::getInstance()->alias('Html', 'Illuminate\Html\HtmlFacade');
			}
		}
	}

	/**
	 * Check if an alias already exists in the IOC
	 * @param $alias
	 * @return bool
	 */
	private function aliasExists($alias)
	{
		return array_key_exists($alias, AliasLoader::getInstance()->getAliases());
	}

	/**
	 * @return array
	 */
	public function provides()
	{
		return [
			'laravel-form-builder',
			'templates'
		];
	}

}
